function FormHandler(sForm) {
  this.form = document.querySelector(sForm);
}
FormHandler.prototype.init = function() {
  this._formStartEvent();
};
FormHandler.prototype._formStartEvent = function() {
  let self = this;
  self.form.addEventListener('submit', _listening);
  function _listening(e) {
    e.preventDefault();
    let link = e.target[0].value;
    const reg = /^(?:([a-z]+):(?:([a-z]*):)?\/\/)?(?:([^:@]*)(?::([^:@]*))?@)?((?:[a-z0-9_-]+\.)+[a-z]{2,}|localhost|(?:(?:[01]?\d\d?|2[0-4]\d|25[0-5])\.){3}(?:(?:[01]?\d\d?|2[0-4]\d|25[0-5])))(?::(\d+))?(?:([^:\?\#]+))?(?:\?([^\#]+))?(?:\#([^\s]+))?$/i
    if(!reg.test(link)) {
      let error = self._createElem('p', 'Invalid link');
      self.form.insertAdjacentElement('afterEnd', error);
      return false;
    }
    fetch(link)
      .then(function(response){return response.json()})
      .then(data => {self.responseFilter(data)})
      .catch(e => {console.log(e)});
  }
};
FormHandler.prototype._createElem = function(tag, text) {
  let elem = document.createElement(tag);
  if(text !== undefined)
    elem.innerText = text;
  return elem;
};
FormHandler.prototype.responseFilter = function(data) {
  const arr1 = [],
    arr2 = [],
    arr3 = [];

  data.map(function(el) {
    let {favoriteFruit, balance, age, eyeColor, gender, isActive} = el;
    if(favoriteFruit === 'banana') {
      arr1.push(el);
    }
    balance = parseInt(balance.replace(/[^\d\.]/gim, ''));
    if(balance > 2000 && parseInt(age) > 25) {
      arr2.push(el);
    }
    if(eyeColor === 'blue' && gender === 'female' && isActive === false) {
      arr3.push(el);
    }

  });
  this.responseRender([arr1, arr2, arr3]);
};
FormHandler.prototype.responseRender = function(data) {
  if(!Array.isArray(data)) {
    let error = this._createElem('p', 'something goes wrong');
    this.form.insertAdjacentElement('afterEnd', error);
    return false;
  }
  const self = this;
  data.map(function(arr, i) {
    let ul = self._createElem('ul');
    document.body.insertAdjacentElement('afterEnd', ul);
    ul.appendChild(self._createElem('li', `arr ${i+1}`));
    arr.map(function(obj) {
      ul.appendChild(self._createElem('li', JSON.stringify(obj)));
    });

  });
};
window.addEventListener('load', main);

function main() {
  const newForm = new FormHandler('#appForm');
  newForm.init();
}